(function(){

  'use strict';

  /**
   * Classes
   */

  class Ball {
    constructor(num = 1) {
      this.el = document.createElement('div');

      var colorClasses = ['blue', 'red', 'green', '']; // empty element is for default color

      this.el.setAttribute('class', 'ball ' + colorClasses[Math.floor(Math.random() * colorClasses.length)]);
      this.el.setAttribute('id', 'ball-' + num);
      this.el.draggable = true;

      this.el.addEventListener('dragstart', function(e){
        e.dataTransfer.setData('text', e.target.id);
      });

      return this.el;
    }
  }

  class BumpingBall {
    constructor(data) {
      this.x = data.x;
      this.y = data.y;
      this.r = data.ballWidth; // radius
      this.dx = 4; // x shift (speed)
      this.dy = 6; // y shift (speed)
      this.c = data.ballColor; // color
    }

    draw() {
      bumpBoxContext.beginPath();
      bumpBoxContext.arc(this.x, this.y, this.r, 0, Math.PI*2,true);
      bumpBoxContext.fillStyle= this.c;
      bumpBoxContext.closePath();
      bumpBoxContext.fill();

      this.x += this.dx;
      this.y += this.dy;

      if( (this.x > bumpBox.width - this.r) || (this.x < this.r)) {
        this.dx = -this.dx;
      }

      if( (this.y > bumpBox.height - this.r) || (this.y < this.r)) {
        this.dy = -this.dy;
      }
    }
  }


  /**
   * Main
   */

  var shelf = document.getElementById('shelf'),
      bumpBox = document.getElementById('bump-box'),
      bumpBoxContext = bumpBox.getContext('2d'),
      numOfBalls = 10,
      bumpingBalls = [];

  function updateCanvas() {
    bumpBoxContext.clearRect(0,0,500, 500); // TODO: unhardcode
    for (let i = 0; i < bumpingBalls.length; i++) {
      bumpingBalls[i].draw();
    }
  }

  window.addEventListener('load', function(){
    shelf.style.maxHeight = bumpBox.height + 'px';
    for (let i=1; i<=numOfBalls; i++) {
        setTimeout(shelf.append(new Ball(i)), 500*i);
      }
  });

  bumpBox.addEventListener('dragover', function(e){
    e.preventDefault();
  });

  bumpBox.addEventListener('drop', function(e){
    e.preventDefault();
    var ballId = e.dataTransfer.getData("text"),
        ball = document.getElementById(ballId),
        ballStyle = window.getComputedStyle(ball),
        dropData = {
          ballWidth: ballStyle.getPropertyValue('width').match(/\d*/) / 2,
          ballColor: ballStyle.getPropertyValue('background-color'),
          x: e.layerX,
          y: e.layerY
        };

    bumpingBalls.push(new BumpingBall(dropData));
    shelf.removeChild(ball);
  });

  setInterval(updateCanvas, 20);
})();
