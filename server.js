//require('look').start(5959, 'localhost');

var http = require('http'),
    express = require('express'),
    reload = require('reload'),
    path = require('path'),
    chokidar = require('chokidar');

var app = express(),
    watcher = chokidar.watch('public');

var pubDir = path.join(__dirname, 'public');

app
  .use(express.static(pubDir))
  .get('/', function (req, res) {
    res.sendFile(path.join(pubDir, 'index.html'));
  });

var server = http.createServer(app),
    reloadServer = reload(server, app);

watcher.on('change', function(path, stats) {
  reloadServer.reload();
});

server.listen(8000);
